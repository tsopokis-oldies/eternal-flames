# Eternal Flames Demo

During the [Gardening 96 Wild Demo Compo](http://www.deus.gr/gardening96/g96rep.html), which took place on the 
30th August & 1st September 1996 - Patras, Greece - [the invitation](http://www.deus.gr/gardening96/compos.htm)
I was part of the team that created the Eternal Flames demo. You can find some information at [demozoo.org](https://demozoo.org/productions/30288/)

![screenshot](resources/eternal-flames.png)

[Video of the demo](https://www.youtube.com/watch?v=RlR1IfdPN24)

## Credits

This intro was created during the gardening 96 party

Code : Dracul, Napalm/Lunatec (jtsop)

Music : Cj DiB

Special thanks to : Miner, Nestor, Daremon, Orpheas, Orthodox
 
## Run the pre-built executable

You need to have a Linux OS and install the DOSbox package

edit ~/.dosbox/dosbox-*.conf

and set cycles=12000 

then run it using DOSbox

## Compile from source

If I ever try to do this, I will update these instructions

## Libraries Used

The audio is played using the [MIDAS Sound System](https://gitlab.com/tsopokis-oldies/midas-sound-system)